package implementation;

import tools.MyALU;
import utilitytypes.EnumOpcode;
import baseclasses.InstructionBase;
import baseclasses.PipelineRegister;
import baseclasses.PipelineStageBase;
import voidtypes.VoidLatch;
import baseclasses.CpuCore;
import baseclasses.Latch;
import cpusimulator.CpuSimulator;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static utilitytypes.EnumOpcode.*;
import utilitytypes.ICpuCore;
import utilitytypes.IGlobals;
import utilitytypes.IModule;
import utilitytypes.IPipeReg;
import static utilitytypes.IProperties.*;
import utilitytypes.IRegFile;
import utilitytypes.Logger;
import utilitytypes.Operand;
import voidtypes.VoidLabelTarget;
import tools.PassthroughPipeStage;

public class Issue_Queue extends PipelineStageBase {

	

	public Issue_Queue(ICpuCore core) {
		super(core, "IQ");
	}
            ArrayList<InstructionBase> Q = new ArrayList<InstructionBase>();
	@Override
	public void compute() {

	ICpuCore core = getCore();
        Latch output;
	Latch input = readInput(0);
	input.consume();
	if (!input.isNull())
	Q.add(input.getInstruction());
		
		


		for (int i = 0; i < Q.size(); i++) {
			InstructionBase ins = Q.get(i);
			EnumOpcode opcode = ins.getOpcode();
			Set<String> SourceFrwd = core.getForwardingSources();
                        boolean yes = false, num1 = false, num2 = false,abc = true,bcd = true;;
	        
			int output_num;
			if (opcode == EnumOpcode.MUL) {
				output_num = lookupOutput("IQTomul");
				output = this.newOutput(output_num);
				
			} else if (opcode == EnumOpcode.DIV) {
				output_num = lookupOutput("IQTodiv");
				output = this.newOutput(output_num);
				
			} else if (opcode == EnumOpcode.FDIV) {
				output_num = lookupOutput("IQTof_div");
				output = this.newOutput(output_num);
				
			} else if ((opcode == EnumOpcode.FADD || opcode == EnumOpcode.FSUB || opcode == EnumOpcode.FCMP)) {
				output_num = lookupOutput("IQTof_asc");
				output = this.newOutput(output_num);
				
			} else if (opcode == EnumOpcode.FMUL) {
				output_num = lookupOutput("IQTof_mul");
				output = this.newOutput(output_num);
				
			} else if (opcode.accessesMemory()) {
				output_num = lookupOutput("IQTomem");
			        output = this.newOutput(output_num);
				
			} else if(!yes){
				output_num = lookupOutput("IQToExecute");
                                output = this.newOutput(output_num);
				yes = true;
			} else continue;
			
			
			if (!outputCanAcceptWork(output_num))
		            return;
			
	       
	        
	        boolean oper0src = opcode.oper0IsSource();

	        Operand oper0 = ins.getOper0();
	        Operand src1  = ins.getSrc1();
	        Operand src2  = ins.getSrc2();
	        // Put operands into array because we will loop over them,
	        // searching the pipeline for forwarding opportunities.
	        Operand[] operArray = {oper0, src1, src2};

	        // For operands that are not registers, getRegisterNumber() will
	        // return -1.  We will use that to determine whether or not to
	        // look for a given register in the pipeline.
	        int[] RegSource = new int[3];
	        // Only want to forward to oper0 if it's a source.
	        RegSource[0] = oper0src ? oper0.getRegisterNumber() : -1;
	        RegSource[1] = src1.getRegisterNumber();
	        RegSource[2] = src2.getRegisterNumber();

	        for (int j=0; j<3; j++) {
	            int RegNum = RegSource[j];
	            // Skip any operands that are not register sources
	            if (RegNum < 0) continue;
	            // Skip any operands that already have values
	            if (operArray[j].hasValue()) continue;
	            Operand oper = operArray[j];
	            String RegName = oper.getRegisterName();
	            String operName = operNames[j];
                    int l = 0;
                    while(l<256)
                    {
                    if( l==255)
                     {
              
                    }
                    l++;
                     }         
	            String sourceFound = null;
	            boolean cycles = false;
                    boolean squashing_instruction = false;
                    IGlobals globals = (GlobalData)getCore().getGlobals();
                for(int p = 0; p<256; p++){
               
                }         
	            looping:
	            for (String fwdpipeReg : SourceFrwd ) {
	                IPipeReg.EnumForwardingStatus FrwdStat = core.matchForwardingRegister(fwdpipeReg, RegNum);

	                switch (FrwdStat) {
	                    case NULL:
	                        break;
	                    case VALID_NOW:
	                        sourceFound = fwdpipeReg;
	                        break looping;
	                    case VALID_NEXT_CYCLE:
	                        sourceFound = fwdpipeReg;
	                        cycles = true;
	                        break looping;
	                }
	            }
        String finalIns = null;            
        int k = 0;
        while(k<256)
        {
            if(k==255)
            {
                addStatusWord("The Issue Queue is Full");
                System.exit(0);
            }
            if(GlobalData.stack[k] == "null")
            {
                //GlobalData.stack[k] = ins.toString();
              //  input.consume();
                break;
            }
            for(k=0; k<256; k++)
            {
                //if(GlobalData.stack[k] != "null") {
                    finalIns = finalIns + GlobalData.stack[k];
                //}
                //setActivity(finalIns);
            }
            for(k=0; k<256; k++){
               // if(GlobalData.stack[k] !="null"){
                //}
                i++;
            }
       }
	            if (sourceFound != null) {
	                if (!cycles) {
	                    // If the register number was found and there is a valid
	                    // result, go ahead and get the value.
	                    int value = core.getResultValue(sourceFound);
	                    boolean isfloat = core.isResultFloat(sourceFound);
	                    operArray[j].setValue(value, isfloat);
                    for(k=0; k<256; k++)
                    {
                //if(GlobalData.stack[k] != "null") {
                    finalIns = finalIns + GlobalData.stack[k];
                //}
                //setActivity(finalIns);
                    }
	                    if (CpuSimulator.printForwarding) {
	                        Logger.out.printf("# Forwarding %s=%s from %s to %s of %s\n", 
	                                RegName, oper.getValueAsString(),
	                                sourceFound, operName,
	                                getHierarchicalName());
	                    }
                            for(k=0; k<256; k++)
            {
                //if(GlobalData.stack[k] != "null") {
                    finalIns = finalIns + GlobalData.stack[k];
                //}
                //setActivity(finalIns);
            }
	                } 
                        
                        else {
	                    // Post forwarding for the next stage on the next cycle by
	                    // setting a property on the latch that specifies which
	                    // operand(s) is forwarded from what pipeline register.
	                    // For instance, setting the property "forward1" to the 
	                    // value "ExecuteToWriteback" will inform the next stage
	                    // to get a value for src1 from ExecuteToWriteback.
	                    String Insname = "forward" + j;
	                    output.setProperty(Insname, sourceFound);
	                    switch(j) {
	                    case 1: num1 = true; 
                                    break;
	                    case 2: num2 = true;    
                                    break;
	                    }
	                }
	            }
	        }
	    	
			if(ins.getSrc1().isRegister() && !ins.getSrc1().hasValue())
				if(!num1)
                                abc = false;
				
			if(ins.getSrc2().isRegister() && !ins.getSrc2().hasValue())
				if(!num2)
				bcd = false;
			for(int k=0; k<256; k++)
            {
                //if(GlobalData.stack[k] != "null") {
                    
                //}
                //setActivity(finalIns);
            }
			if(!abc || !bcd) continue;
			output.setInstruction(ins);
			output.write();
			Q.remove(ins);
			i--;

		}

	}

  }

